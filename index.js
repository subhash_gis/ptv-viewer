var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');
closer.onclick = function() {
    container.style.display = 'none';
    closer.blur();
    return false;
};
var overlayPopup = new ol.Overlay({
    element: container
});

var view = new ol.View({
    center: [0, 0],
    zoom: 7,
    maxZoom: 32,
    minZoom: 1
});

var pointZoom = 16;

var map = new ol.Map({
    controls: [
        new ol.control.ScaleLine({
            "minWidth": 64,
            "units": "metric"
        }),
        new ol.control.LayerSwitcher({
            "showZoomTo": true,
            "allowReordering": true,
            "showGroupContent": false,
            "showDownload": false,
            "showOpacity": true,
            "tipLabel": "Layers"
        }),
        new ol.control.Geolocation(),
        new ol.control.OverviewMap({
            collapsed: true
        }),
        new ol.control.MousePosition({
            "projection": "EPSG:4326",
            "undefinedHTML": "&nbsp;",
            "coordinateFormat": ol.coordinate.createStringXY(4)
        }),
        new ol.control.Rotate({
            autoHide: true
        }),
        new ol.control.FullScreen(),
        new ol.control.Zoom({
            "zoomInTipLabel": "Zoom in",
            "zoomOutLabel": "-",
            "zoomOutTipLabel": "Zoom out",
            "duration": 250,
            "zoomInLabel": "+",
            "delta": 1.2
        }),
        new ol.control.Attribution()
    ],
    target: document.getElementById('map'),
    renderer: 'canvas',
    overlays: [overlayPopup],
    layers: layersList,
    view: view

});

// function mapLayerChanged(event) {
//     //alert(event.type + " " + event.layer.name);
//     alert("Working");
// }
// map.on('change', mapLayerChanged);


var originalExtent = [16079432.280792, -4650296.903953, 16274075.447941, -4495471.946524];
map.getView().fitExtent(originalExtent, map.getSize());

var selectInteraction = new ol.interaction.Select({
    layers: function(layer) {
        return selectableLayersList.indexOf(layer) != -1;
    },
    style: new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(255, 100, 50, 0.3)'
        }),
        stroke: new ol.style.Stroke({
            width: 2,
            color: 'rgba(255, 100, 50, 0.8)'
        }),
        image: new ol.style.Circle({
            fill: new ol.style.Fill({
                color: 'rgba(255, 100, 50, 0.5)'
            }),
            stroke: new ol.style.Stroke({
                width: 2,
                color: 'rgba(255, 100, 50, 0.8)'
            }),
            radius: 7
        })
    }),
    toggleCondition: ol.events.condition.shiftKeyOnly,
    filter: function(feature, layer) {
        features = feature.get("features")
        if (features) {
            return false;
        } else {
            return true
        }
    }
});
map.addInteraction(selectInteraction);

isDuringMultipleSelection = false;

var selectedFeatures = selectInteraction.getFeatures();
selectedFeatures.clear = function() {
    isDuringMultipleSelection = true;
    while (this.getLength() > 1) {
        this.pop();
    }
    isDuringMultipleSelection = false;
    if (this.getLength()) {
        this.pop();
    }
}


var currentInteraction;



popupLayers = [``, `<b>STATION</b>: [STATION]<br><b>COM_CAPAC</b>: [COM_CAPAC]<br><b>UFI</b>: [UFI]<br><b>UFI_CR</b>: [UFI_CR]`, `<b>MET_STOPID</b>: [MET_STOPID]<br><b>STOPSPEC</b>: [STOPSPEC]<br><b>ZONES</b>: [ZONES]<br><b>RTUSESTOP</b>: [RTUSESTOP]<br><b>UFI</b>: [UFI]<br><b>UFI_CR</b>: [UFI_CR]`, `<b>DESCSHORT</b>: [DESCSHORT]<br><b>DIR_DESC</b>: [DIR_DESC]<br><b>LINEFROM</b>: [LINEFROM]<br><b>LINETO</b>: [LINETO]<br><b>LINEVIA</b>: [LINEVIA]<br><b>LINEDESCLG</b>: [LINEDESCLG]<br><b>RTPATHDESC</b>: [RTPATHDESC]<br><b>ROUTEPTHFR</b>: [ROUTEPTHFR]<br><b>ROUTEPTHTO</b>: [ROUTEPTHTO]<br><b>ROUTEPTHVI</b>: [ROUTEPTHVI]<br><b>OPERATOR</b>: [OPERATOR]<br><b>UFI</b>: [UFI]<br><b>UFI_CR</b>: [UFI_CR]`, `<b>UFI</b>: [UFI]<br><b>UFI_CR</b>: [UFI_CR]`, `<b>FACILITY</b>: [FACILITY]<br><b>GAUGE</b>: [GAUGE]<br><b>ELECTRIC</b>: [ELECTRIC]<br><b>UFI</b>: [UFI]<br><b>UFI_CR</b>: [UFI_CR]`, `<b>STATION</b>: [STATION]<br><b>TYPE</b>: [TYPE]<br><b>CAPACITY</b>: [CAPACITY]<br><b>UFI</b>: [UFI]<br><b>UFI_CR</b>: [UFI_CR]`, `<b>MET_STOPID</b>: [MET_STOPID]<br><b>STATION</b>: [STATION]<br><b>STATIONTYP</b>: [STATIONTYP]<br><b>ZONES</b>: [ZONES]<br><b>STOPMODE</b>: [STOPMODE]<br><b>UFI</b>: [UFI]<br><b>UFI_CR</b>: [UFI_CR]`];

var highlightOverlay = new ol.FeatureOverlay({
    map: map,
    style: [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: '#f00',
            width: 1
        }),
        fill: new ol.style.Fill({
            color: 'rgba(255,0,0,0.1)'
        }),
    })]
});

var doHighlight = false;
var doHover = false;

var decluster = function(f) {
    features = f.get("features")
    if (features) {
        if (features.length > 1) {
            return null;
        } else {
            return features[0];
        }
    } else {
        return f;
    }
}

var highlight;
var onPointerMove = function(evt) {
    if (!doHover && !doHighlight) {
        return;
    }
    var pixel = map.getEventPixel(evt.originalEvent);
    var coord = evt.coordinate;
    var popupText = "";
    var currentFeature;
    var toAdd = [];
    map.forEachFeatureAtPixel(pixel, function(feature, layer) {
        feature = decluster(feature);
        if (feature) {
            currentFeature = feature;
            if (popupText == "") {
                popupText = popupLayers[singleLayersList.indexOf(layer)];
                if (popupText) {
                    var currentFeatureKeys = currentFeature.getKeys();
                    for (var i = 0; i < currentFeatureKeys.length; i++) {
                        if (currentFeatureKeys[i] != 'geometry') {
                            var value = currentFeature.get(currentFeatureKeys[i]);
                            if (value) {
                                popupText = popupText.replace("[" + currentFeatureKeys[i] + "]",
                                    String(currentFeature.get(currentFeatureKeys[i])))
                            }
                        }
                    }
                }
            }
        }
    });

    if (doHighlight) {
        if (currentFeature !== highlight) {
            if (highlight) {
                highlightOverlay.removeFeature(highlight);
            }
            if (currentFeature) {
                highlightOverlay.addFeature(currentFeature);
            }
            highlight = currentFeature;
        }
    }

    if (doHover) {
        if (popupText) {
            overlayPopup.setPosition(coord);
            content.innerHTML = popupText;
            container.style.display = 'block';
        } else {
            container.style.display = 'none';
            closer.blur();
        }
    }
};

var onSingleClick = function(evt) {
    if (doHover) {
        return;
    }
    var pixel = map.getEventPixel(evt.originalEvent);
    var coord = evt.coordinate;
    var popupText = "";
    var currentFeature;
    var toAdd = [];
    map.forEachFeatureAtPixel(pixel, function(feature, layer) {
        feature = decluster(feature);
        if (feature) {
            currentFeature = feature;
            if (popupText == "") {
                popupText = popupLayers[singleLayersList.indexOf(layer)];
                if (popupText) {
                    var currentFeatureKeys = currentFeature.getKeys();
                    for (var i = 0; i < currentFeatureKeys.length; i++) {
                        if (currentFeatureKeys[i] != 'geometry') {
                            var value = currentFeature.get(currentFeatureKeys[i]);
                            if (value) {
                                popupText = popupText.replace("[" + currentFeatureKeys[i] + "]",
                                    String(currentFeature.get(currentFeatureKeys[i])))
                            }
                        }
                    }
                }
            }
        }
    });
    if (currentInteraction == null) {
        selectedFeatures.clear();
        if (toAdd.length !== 0) {
            isDuringMultipleSelection = true;
            selectedFeatures.extend(toAdd.slice(0, -1));
            isDuringMultipleSelection = false;
            selectedFeatures.push(toAdd[toAdd.length - 1]);
        }
    }
    if (popupText) {
        overlayPopup.setPosition(coord);
        content.innerHTML = popupText;
        container.style.display = 'block';
    } else {
        container.style.display = 'none';
        closer.blur();
    }
};
map.on('pointermove', function(evt) {
    onPointerMove(evt);
});
map.on('singleclick', function(evt) {
    onSingleClick(evt);
});