var baseLayer = new ol.layer.Group({
    'type': 'base',
    'title': 'Base maps',
    layers: [new ol.layer.Tile({
        type: 'base',
        title: 'MapQuest aerial',
        source: new ol.source.MapQuest({
            layer: 'sat'
        })
    }), new ol.layer.Tile({
        type: 'base',
        title: 'MapQuest roads',
        source: new ol.source.MapQuest({
            layer: 'osm'
        })
    })]
});
var lyr_melbourne_region = new ol.layer.Vector({
    source: new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojson_melbourne_region)
    }),

    style: style_melbourne_region,
    title: "Melbourne Region"
});
var lyr_traincarpark = new ol.layer.Vector({
    source: new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojson_traincarpark)
    }),

    style: style_traincarpark,
    title: "Train Car Park"
});
var cluster_bus_stop = new ol.source.Cluster({
    distance: 40.0,
    source: new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojson_bus_stop)
    }),
});
var lyr_bus_stop = new ol.layer.Vector({
    source: cluster_bus_stop,
    style: style_bus_stop,
    title: "Bus Stop"
});
var lyr_busroute = new ol.layer.Vector({
    source: new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojson_busroute)
    }),

    style: style_busroute,
    title: "Bus Route"
});
var lyr_tramtracks = new ol.layer.Vector({
    source: new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojson_tramtracks)
    }),

    style: style_tramtracks,
    title: "Tram Tracks"
});
var lyr_traintrack = new ol.layer.Vector({
    source: new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojson_traintrack)
    }),

    style: style_traintrack,
    title: "Train Track"
});
var lyr_trainstationbikestorage = new ol.layer.Vector({
    source: new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojson_trainstationbikestorage)
    }),

    style: style_trainstationbikestorage,
    title: "Train Station Bike Storage"
});
var lyr_trainstations = new ol.layer.Vector({
    source: new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojson_trainstations)
    }),

    style: style_trainstations,
    title: "Train Stations"
});

lyr_melbourne_region.setVisible(true);
lyr_traincarpark.setVisible(false);
lyr_bus_stop.setVisible(true);
lyr_busroute.setVisible(true);
lyr_tramtracks.setVisible(true);
lyr_traintrack.setVisible(true);
lyr_trainstationbikestorage.setVisible(false);
lyr_trainstations.setVisible(true);
var layersList = [baseLayer, lyr_melbourne_region, lyr_traincarpark, lyr_bus_stop, lyr_busroute, lyr_tramtracks, lyr_traintrack, lyr_trainstationbikestorage, lyr_trainstations];
var singleLayersList = [lyr_melbourne_region, lyr_traincarpark, lyr_bus_stop, lyr_busroute, lyr_tramtracks, lyr_traintrack, lyr_trainstationbikestorage, lyr_trainstations];
var selectableLayersList = [lyr_traincarpark, lyr_bus_stop, lyr_busroute, lyr_tramtracks, lyr_traintrack, lyr_trainstationbikestorage, lyr_trainstations];