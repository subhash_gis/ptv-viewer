var textStyleCache_melbourne_region = {}
var clusterStyleCache_melbourne_region = {}
var style_melbourne_region = function(feature, resolution) {

    var value = ""
    var style = [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(0,0,0,1.0)",
            lineDash: null,
            width: 3
        }),
        fill: new ol.style.Fill({
            color: "rgba(0,0,0,0.3)"
        })
    })];
    var labelText = "";
    var key = value + "_" + labelText

    if (!textStyleCache_melbourne_region[key]) {
        var text = new ol.style.Text({
            font: '26.0px Calibri,sans-serif',
            text: labelText,
            fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
            }),
        });
        textStyleCache_melbourne_region[key] = new ol.style.Style({
            "text": text
        });
    }
    var allStyles = [textStyleCache_melbourne_region[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};