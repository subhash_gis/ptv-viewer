var textStyleCache_traincarpark = {}
var clusterStyleCache_traincarpark = {}
var style_traincarpark = function(feature, resolution) {

    var value = ""
    var style = [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(0,0,0,1.0)",
            lineDash: null,
            width: 0
        }),
        fill: new ol.style.Fill({
            color: "rgba(11,155,227,1.0)"
        })
    })];
    var labelText = "";
    var key = value + "_" + labelText

    if (!textStyleCache_traincarpark[key]) {
        var text = new ol.style.Text({
            font: '26.0px Calibri,sans-serif',
            text: labelText,
            fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
            }),
        });
        textStyleCache_traincarpark[key] = new ol.style.Style({
            "text": text
        });
    }
    var allStyles = [textStyleCache_traincarpark[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};