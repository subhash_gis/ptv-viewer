var categories_trainstationbikestorage = {
    "Cages": [new ol.style.Style({
        image: new ol.style.Circle({
            radius: 4.0,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,255)',
                lineDash: null,
                width: 0
            }),
            fill: new ol.style.Fill({
                color: "rgba(58,201,211,1.0)"
            })
        })
    })],
    "Hoops": [new ol.style.Style({
        image: new ol.style.Circle({
            radius: 4.0,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,255)',
                lineDash: null,
                width: 0
            }),
            fill: new ol.style.Fill({
                color: "rgba(222,232,72,1.0)"
            })
        })
    })],
    "Locker": [new ol.style.Style({
        image: new ol.style.Circle({
            radius: 4.0,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,255)',
                lineDash: null,
                width: 0
            }),
            fill: new ol.style.Fill({
                color: "rgba(228,74,63,1.0)"
            })
        })
    })],
    "Other": [new ol.style.Style({
        image: new ol.style.Circle({
            radius: 4.0,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,255)',
                lineDash: null,
                width: 0
            }),
            fill: new ol.style.Fill({
                color: "rgba(235,43,219,1.0)"
            })
        })
    })],
    "Parkiteer": [new ol.style.Style({
        image: new ol.style.Circle({
            radius: 4.0,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,255)',
                lineDash: null,
                width: 0
            }),
            fill: new ol.style.Fill({
                color: "rgba(121,115,218,1.0)"
            })
        })
    })]
};
var textStyleCache_trainstationbikestorage = {}
var clusterStyleCache_trainstationbikestorage = {}
var style_trainstationbikestorage = function(feature, resolution) {

    var value = feature.get("TYPE");
    var style = categories_trainstationbikestorage[value];
    var labelText = "";
    var key = value + "_" + labelText

    if (!textStyleCache_trainstationbikestorage[key]) {
        var text = new ol.style.Text({
            font: '26.0px Calibri,sans-serif',
            text: labelText,
            fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
            }),
        });
        textStyleCache_trainstationbikestorage[key] = new ol.style.Style({
            "text": text
        });
    }
    var allStyles = [textStyleCache_trainstationbikestorage[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};