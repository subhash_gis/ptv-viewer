var textStyleCache_bus_stop = {}
var clusterStyleCache_bus_stop = {}
var style_bus_stop = function(feature, resolution) {
    var size = feature.get('features').length;
    if (size != 1) {
        var style = clusterStyleCache_bus_stop[size];
        if (!style) {
            style = [new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 10,
                    stroke: new ol.style.Stroke({
                        color: '#fff'
                    }),
                    fill: new ol.style.Fill({
                        color: '#3399CC'
                    })
                }),
                text: new ol.style.Text({
                    text: size.toString(),
                    fill: new ol.style.Fill({
                        color: '#fff'
                    })
                })
            })];
            clusterStyleCache_bus_stop[size] = style;
        }
        return style;
    }
    var value = ""
    var style = [new ol.style.Style({
        image: new ol.style.Circle({
            radius: 6.0,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,255)',
                lineDash: null,
                width: 1
            }),
            fill: new ol.style.Fill({
                color: "rgba(255,255,255,1.0)"
            })
        })
    })];
    var labelText = "";
    var key = value + "_" + labelText

    if (!textStyleCache_bus_stop[key]) {
        var text = new ol.style.Text({
            font: '26.0px Calibri,sans-serif',
            text: labelText,
            fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
            }),
        });
        textStyleCache_bus_stop[key] = new ol.style.Style({
            "text": text
        });
    }
    var allStyles = [textStyleCache_bus_stop[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};