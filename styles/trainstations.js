var categories_trainstations = {
    "": [new ol.style.Style({
        image: new ol.style.Circle({
            radius: 4.0,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,255)',
                lineDash: null,
                width: 1
            }),
            fill: new ol.style.Fill({
                color: "rgba(182,126,222,1.0)"
            })
        })
    })],
    "Host": [new ol.style.Style({
        image: new ol.style.Circle({
            radius: 4.0,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,255)',
                lineDash: null,
                width: 0
            }),
            fill: new ol.style.Fill({
                color: "rgba(121,204,66,1.0)"
            })
        })
    })],
    "Premium": [new ol.style.Style({
        image: new ol.style.Circle({
            radius: 4.0,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,255)',
                lineDash: null,
                width: 0
            }),
            fill: new ol.style.Fill({
                color: "rgba(255,230,0,1.0)"
            })
        })
    })],
    "Unmanned": [new ol.style.Style({
        image: new ol.style.Circle({
            radius: 4.0,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,255)',
                lineDash: null,
                width: 0
            }),
            fill: new ol.style.Fill({
                color: "rgba(222,105,95,1.0)"
            })
        })
    })],
    "NULL": [new ol.style.Style({
        image: new ol.style.Circle({
            radius: 4.0,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,255)',
                lineDash: null,
                width: 0
            }),
            fill: new ol.style.Fill({
                color: "rgba(65,179,88,1.0)"
            })
        })
    })]
};
var textStyleCache_trainstations = {}
var clusterStyleCache_trainstations = {}
var style_trainstations = function(feature, resolution) {

    var value = feature.get("STATIONTYP");
    var style = categories_trainstations[value];
    var labelText = "";
    var key = value + "_" + labelText

    if (!textStyleCache_trainstations[key]) {
        var text = new ol.style.Text({
            font: '26.0px Calibri,sans-serif',
            text: labelText,
            fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
            }),
        });
        textStyleCache_trainstations[key] = new ol.style.Style({
            "text": text
        });
    }
    var allStyles = [textStyleCache_trainstations[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};