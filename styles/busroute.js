var textStyleCache_busroute = {}
var clusterStyleCache_busroute = {}
var style_busroute = function(feature, resolution) {

    var value = ""
    var style = [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(0,0,0,1.0)",
            lineDash: null,
            width: 6
        })
    }), new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(255,255,255,1.0)",
            lineDash: null,
            width: 3
        })
    })];
    var labelText = "";
    var key = value + "_" + labelText

    if (!textStyleCache_busroute[key]) {
        var text = new ol.style.Text({
            font: '26.0px Calibri,sans-serif',
            text: labelText,
            fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
            }),
        });
        textStyleCache_busroute[key] = new ol.style.Style({
            "text": text
        });
    }
    var allStyles = [textStyleCache_busroute[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};