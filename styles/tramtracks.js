var textStyleCache_tramtracks = {}
var clusterStyleCache_tramtracks = {}
var style_tramtracks = function(feature, resolution) {

    var value = ""
    var style = [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(122,245,0,1.0)",
            lineDash: null,
            width: 3
        })
    }), new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(85,170,0,1.0)",
            lineDash: [3],
            width: 3
        })
    })];
    var labelText = "";
    var key = value + "_" + labelText

    if (!textStyleCache_tramtracks[key]) {
        var text = new ol.style.Text({
            font: '26.0px Calibri,sans-serif',
            text: labelText,
            fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
            }),
        });
        textStyleCache_tramtracks[key] = new ol.style.Style({
            "text": text
        });
    }
    var allStyles = [textStyleCache_tramtracks[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};