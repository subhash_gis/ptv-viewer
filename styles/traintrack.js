var textStyleCache_traintrack = {}
var clusterStyleCache_traintrack = {}
var style_traintrack = function(feature, resolution) {

    var value = ""
    var style = [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(170,110,142,1.0)",
            lineDash: null,
            width: 4
        })
    }), new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(255,255,255,1.0)",
            lineDash: [3],
            width: 3
        })
    })];
    var labelText = "";
    var key = value + "_" + labelText

    if (!textStyleCache_traintrack[key]) {
        var text = new ol.style.Text({
            font: '26.0px Calibri,sans-serif',
            text: labelText,
            fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
            }),
        });
        textStyleCache_traintrack[key] = new ol.style.Style({
            "text": text
        });
    }
    var allStyles = [textStyleCache_traintrack[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};